const eCommerceData = {
    wallet: 100000,
    products: [
        { productImage: 'img/winter-coat-m.png', productTitle: 'Winter Coat', productPrice: 7800 },
        { productImage: 'img/winter-coat-w.png', productTitle: 'Winter Coat', productPrice: 7800 },
        { productImage: 'img/t-shirt-w.png', productTitle: 'Plain T-Shirt', productPrice: 3450 },
        { productImage: 'img/trousers.png', productTitle: 'Trousers', productPrice: 4600 },
        { productImage: 'img/t-shirt-m.png', productTitle: 'Plain T-Shirt', productPrice: 3450 },
        { productImage: 'img/winter-coat-m.png', productTitle: 'Winter Coat', productPrice: 7800 },
        { productImage: 'img/winter-coat-w.png', productTitle: 'Winter Coat', productPrice: 7800 },
        { productImage: 'img/t-shirt-w.png', productTitle: 'Plain T-Shirt', productPrice: 3450 },
        { productImage: 'img/trousers.png', productTitle: 'Trousers', productPrice: 4600 },
        { productImage: 'img/t-shirt-m.png', productTitle: 'Plain T-Shirt', productPrice: 3450 },
        { productImage: 'img/winter-coat-m.png', productTitle: 'Winter Coat', productPrice: 7800 },
        { productImage: 'img/winter-coat-w.png', productTitle: 'Winter Coat', productPrice: 7800 },
        { productImage: 'img/t-shirt-w.png', productTitle: 'Plain T-Shirt', productPrice: 3450 },
        { productImage: 'img/trousers.png', productTitle: 'Trousers', productPrice: 4600 },
        { productImage: 'img/t-shirt-m.png', productTitle: 'Plain T-Shirt', productPrice: 3450 }
    ]
}
const  prod = document.getElementById('prod');
const modal = document.getElementById('modal');
const badge = document.getElementById('badge');
let totalCart = 0;
let wallet = eCommerceData.wallet;
let cart = [];

const backToShop = () => {
    modal.classList.toggle("visible");
}
const checkoutDone = () => {
    wallet = wallet - totalCart;
    walletInfo = `<div>Your Shopping has been successfully done </div>
    <div>Total price: <span>`+ totalCart +`</span></div>
    <div>Wallet: <span>`+ wallet +`</span></div>`

    modal.innerHTML = walletInfo;
    cart = [];
} 
const addToCart = (image, title, price) => {
    cart.push({
        image: image,
        title: title,
        price: price
    })

}

const showCart = () => {
    let cartItems = '<div>Empty cart</div>';
    if(cart.length > 0){
        cartItems = '';

        cart.forEach(function (cartItem) {
            totalCart += cartItem.price;
            cartItems += `<div class="product_cart">
                            <article class="product_cart_holder">
                                <div class="product_cart_title clearfix">
                                    <div class="cart_item">
                                    <img src=`+ cartItem.image + ` alt=`+ cartItem.title + ` class="responsive-img" /></div>
                                    <h3>`+ cartItem.title + `</h3>
                                    <span>`+ cartItem.price + `</span>
                                </div>
                            </article>
                        </div>`
        })
        cartItems += `<div class="total">Total: <span>`+ totalCart +`</span></div>`;
        cartItems += `<div class="checkout">
                <span class="bck"><a href="javascript:void(0)" onClick="backToShop()" >Back</a></span>
                <span class="chek"><a href="javascript:void(0)" onClick="checkoutDone()" >Checkout</a> </span>
                
            </div>`;
    }
    modal.innerHTML = cartItems;
    modal.classList.toggle("visible");
}

const allProdInfo = () => {
    let info = '';
    eCommerceData.products.forEach(function (producinfo) {
        info += `<div class="product">
        <article class="product_holder">
            <div class="image_holder">
                <a href="#">
                    <img src="`+ producinfo.productImage + `" alt="` + producinfo.productTitle + `" class="responsive-img">
                </a>
                <span class="icon_shop_bag"><a href="javascript:void(0)" onClick="addToCart('`+ producinfo.productImage + `','`+ producinfo.productTitle + `',` + producinfo.productPrice + `)">Add to cart</a></span>
            </div>
            <div class="product_title clearfix">
                <h3>`+ producinfo.productTitle + `</h3>
                <span>`+ producinfo.productPrice + ` $</span>
            </div>
        </article>
    </div>`
    });
    return info;
}
prod.innerHTML = allProdInfo();